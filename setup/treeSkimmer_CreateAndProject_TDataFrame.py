#!/usr/bin/env python
from array import *
import os, copy, sys, re
import ROOT
ROOT.gROOT.SetBatch(True)
#ROOT.gROOT.ProcessLineSync('.L tools/customFunctions.C+')
ROOT.gROOT.ProcessLineSync('.L tools/upgradeCustomFunctions.C+')
# ---------------------------------------------------------------------------------
def pumodeToInt(pumode) :
    pumodeint = -999; # initialize as "none" just in case!
    if pumode == "none"       : pumodeint = -999;
    # Total-Lumi
    elif pumode == "nominal"  : pumodeint = 0;
    elif pumode == "up"       : pumodeint = 1;
    elif pumode == "down"     : pumodeint =-1;
    # By-Era
    elif pumode == "nominalA" : pumodeint = 100;
    elif pumode == "upA"      : pumodeint = 101;
    elif pumode == "downA"    : pumodeint = 99;
    elif pumode == "nominalB" : pumodeint = 200;
    elif pumode == "upB"      : pumodeint = 201;
    elif pumode == "downB"    : pumodeint = 199;
    elif pumode == "nominalC" : pumodeint = 300;
    elif pumode == "upC"      : pumodeint = 301;
    elif pumode == "downC"    : pumodeint = 299;
    elif pumode == "nominalD" : pumodeint = 400;
    elif pumode == "upD"      : pumodeint = 401;
    elif pumode == "downD"    : pumodeint = 399;
    elif pumode == "nominalE" : pumodeint = 500;
    elif pumode == "upE"      : pumodeint = 501;
    elif pumode == "downE"    : pumodeint = 499;
    elif pumode == "nominalF" : pumodeint = 600;
    elif pumode == "upF"      : pumodeint = 601;
    elif pumode == "downF"    : pumodeint = 599;
    elif pumode == "nominalG" : pumodeint = 700;
    elif pumode == "upG"      : pumodeint = 701;
    elif pumode == "downG"    : pumodeint = 699;
    elif pumode == "nominalH" : pumodeint = 800;
    elif pumode == "upH"      : pumodeint = 801;
    elif pumode == "downH"    : pumodeint = 799;
    #
    return pumodeint;
    
    
def treeSkimmer_CreateAndProject(pset):
    #
    print "\nBEGIN:: treeSkimmer_CreateTHn\n"
    print "pset.inputIndex: ",pset.inputIndex
    #
    directoryName = "runPlotter_"+pset.analysis
    if not os.path.exists(directoryName): os.makedirs(directoryName)
    #
    if "p" in pset.mode :
        outputHistoFileName = ""
        if pset.inputIndex == -1 : outputHistoFileName = ""
        else                     : outputHistoFileName = "Input_"+str(pset.inputIndex)+"_"
        outputHistoFileName = directoryName+"/"+outputHistoFileName+"TH1.root"
        outputHistoFile=ROOT.TFile(outputHistoFileName,"RECREATE")
        print "\nOutput file for histograms: "+outputHistoFileName,"\n"

        
    # -------------------------------------
    # Extract non-vector branches (from the first available file)
    # -------------------------------------
    nonvectorbranches = []
    for iinput, (bundle,xsec,mfilename,normfudge,relflatunc,technicalselection,treename,isData) in enumerate(pset.inputs):
        mfile=ROOT.TFile(mfilename,"READ")
        tr = mfile.Get(treename)
        for ibranch in tr.GetListOfBranches():
            if "ROOT.TBranchElement" not in str(tr.GetBranch(ibranch.GetName())) : #skip vectors
                nonvectorbranches.append(ibranch.GetName())
        break
        
    # -------------------------------------
    # Parse & alias the input "variables"
    # -------------------------------------
    aliases1D          = [x[0] for x in pset.aliases]           # this is a simple list of "variables" defined in pset.aliases
    aliasesdict        = dict((x, y) for x, y in pset.aliases)  # this is a dictionary of aliases variables
    parsedvariables2D  = [] 
    for ivar,(var,nbin,binlow,binhigh,xlabel,isDiscrete) in enumerate(pset.variables):
        if "[]" in var :
            var_=var.replace("[]","")+"_Vector_"
            parsedvariables2D.append( (str(var_),var.replace("[]","")) )
        elif "[" in var and "]" in var :
            var_=var.replace("]","")
            var_=var_.split("["); 
            branch = var_[0]
            index  = var_[1]
            var_=str(var_[0])+"_"+str(var_[1])+"_"
            if   isDiscrete : parsedvariables2D.append( (var_, "get("+branch+","+str(index)+")") )
            else            : parsedvariables2D.append( (var_, "get("+branch+","+str(index)+")") )
        elif var in aliases1D : 
            var_= str(var)
            parsedvariables2D.append( (var_,str(aliasesdict[var])) )
        elif var in nonvectorbranches :
            var_=str(var)+"_0_"
            parsedvariables2D.append( (var_,str(var)) )
        else :
            print("\nRequested variable \""+str(var)+"\" is not available (neither an alias nor a branch)! exiting..\n")
            exit(0)
    parsedvariables1D = [x[0] for x in parsedvariables2D]

    # -------------------------------------
    # Loop over inputs
    # -------------------------------------
    for iinput, (bundle,xsec,mfilename,normfudge,relflatunc,technicalselection,treename,isData) in enumerate(pset.inputs):
        #
        print ""
        print ""
        print "----------------------------------------------------------------------------"
        print "  Bundle: ",bundle
        print "Filename: ",mfilename
        #
        # -------------------------------------
        # Define input file, tree, and data frame
        # -------------------------------------
        #######ROOT.EnableImplicitMT() #multithreading, do not use due to multithread safety issues!!
        RDF = ROOT.RDataFrame
        globalselection = "(1>0)" if pset.globalselection == "1" else pset.globalselection
        globalandtechnicalselection = globalselection+"&&("+technicalselection+")"
        #########
        #########
        if "c" in pset.mode :
            vanillatree = RDF(treename,mfilename)
            mfile=ROOT.TFile(mfilename,"READ")
            tr = mfile.Get(treename)
            # -------------------------------------
            # First apply fraction restriction (i.e. run over subset of events) if specified.
            if   0.0 < pset.mcfraction and pset.mcfraction < 1.0 and pset.fraction == 1.0 and not isData :
                mcfracNumberOfEvents = int(pset.mcfraction*(tr.GetEntries()))
                subsettree     = vanillatree.Range(0,mcfracNumberOfEvents,1)
            elif 0.0 < pset.fraction and pset.fraction < 1.0 and pset.mcfraction == 1.0 :
                fracNumberOfEvents = int(pset.fraction*(tr.GetEntries()))
                subsettree     = vanillatree.Range(0,fracNumberOfEvents,1)
            else :
                subsettree     = vanillatree
            # -------------------------------------
            # Define aliases to the subset tree
            # -------------------------------------
            #print " \n Realiased variables: ",parsedvariables2D,"\n"
            subsetaliastree = subsettree
            branchListToSave    = ROOT.vector('string')()
            for alias in pset.aliases:
                subsetaliastree = subsetaliastree.Define(alias[0],alias[1]) # define all aliases
            for parsedalias in parsedvariables2D:
                branchListToSave.push_back(parsedalias[0]) # save all variables
                if parsedalias[0] not in aliases1D: subsetaliastree = subsetaliastree.Define(parsedalias[0],parsedalias[1])  # this prevents double-definition of the same variable
            # -------------------------------------
            # Add existing branches to the save-list aka "passThru" mode
            # -------------------------------------
            if   isData : branchesToSave = pset.branchesToSave + pset.branchesToSaveDataOnly
            else        : branchesToSave = pset.branchesToSave + pset.branchesToSaveMCOnly
            for existingbranch in branchesToSave :
                branchListToSave.push_back(existingbranch)
            # -------------------------------------
            # Then  apply global & technical selection to the input
            # -------------------------------------
            globalskimtree = subsetaliastree.Filter(globalandtechnicalselection)
            # -------------------------------------
            # Extract nloweights, commonMC and bundle specific weights
            # -------------------------------------
            commonMCWeights = "(1.0)"
            bundleWeights   = "(1.0)"
            if   pset.nloweightName and not isData : nloWeight = "(nloWeight("+pset.nloweightName+"))"
            else                                   : nloWeight = "(1.0)"
            if not isData :
                for icommonMCWeight,commonMCWeight in enumerate(pset.commonMCWeights):
                    commonMCWeights = commonMCWeights+"*("+commonMCWeight+")"
            for ibundleWeights,bundleWeight in enumerate(pset.bundleWeights):
                if bundle != bundleWeight[0] : continue
                bundleWeights = bundleWeights+"*("+bundleWeight[1]+")"

            # -------------------------------------
            # Extract PU weight
            # -------------------------------------
            puWeight = "(1.0)"
            #puProfileMCvector = ROOT.vector('double')()
            puProfileMC = []
            print "\npset.hpuName: ",pset.hpuName
            if not isData and pset.hpuName : 
                if pset.hpuName == "none" : continue
                # Extract PU profile
                hpu        = mfile.Get(pset.hpuName)
                puintegral = hpu.Integral()
                overflow   = 0.
                underflow  = 0.
                for ibin in xrange(1,hpu.GetNbinsX()+1):
                    if hpu.GetBinCenter(ibin)>98.5: overflow  = overflow  + hpu.GetBinContent(ibin); continue
                    if hpu.GetBinCenter(ibin)<-0.5: underflow = underflow + hpu.GetBinContent(ibin); continue
                    puProfileMC.append(hpu.GetBinContent(ibin)/puintegral)
                puProfileMC[0]  = puProfileMC[0]+underflow/puintegral # first bin
                puProfileMC[-1] = puProfileMC[-1]+overflow/puintegral #  last bin
                print "Minbias Xsection for data profile (pileupCalc.py): ",pset.pumode
                print "Pileup profile from MC is extracted (these are the GenTruePU PDF values, not weights!): "
                print "Length of puProfileMC: ",len(puProfileMC)
                print "Integral including over & underflow: ",puintegral
                print "Overflow(>"+str(hpu.GetNbinsX())+"): ",overflow
                print "Underflow(<0): ",underflow
                print "puProfileMC: ",puProfileMC
                print
                #--
                #for x in puProfileMC: puProfileMCvector.push_back(x)
                puProfileMCstring="{"
                for x in puProfileMC: puProfileMCstring=puProfileMCstring+str(x)+","
                puProfileMCstring=puProfileMCstring[:-1]+"}"
                # mode to int conversions
                pumodeint = pumodeToInt(pset.pumode)
                puWeight = "(pileupWeight("+str(pset.gentruepuweight)+","+str(pumodeint)+","+puProfileMCstring+"))"
            # -------------------------------------
            # Extract MC normalization weight
            # -------------------------------------
            lumiWeight = "(1.0)"
            if pset.hcountName and not isData : #and "c" in pset.mode:
                hcount=mfile.Get(pset.hcountName)
                allevents     = hcount.GetBinContent(1)
                passevents    = hcount.GetBinContent(2)
                if hcount.GetBinContent(3) > 0 : allevents  = hcount.GetBinContent(3) # negative weighted events have been subtracted accordingly here
                if hcount.GetBinContent(3) > 0 : passevents = hcount.GetBinContent(4) # negative weighted events have been subtracted accordingly here
                #--
                print "EventCounter histo (all/pass/nloAll/nloPass): ",int(hcount.GetBinContent(1)),"/",int(hcount.GetBinContent(2)),"/",int(hcount.GetBinContent(3)),"/",int(hcount.GetBinContent(4))
                #--
                lumiweight = 0.0;
                if pset.mcfraction == 1.0 :
                    lumiweight = (pset.lumi*xsec*normfudge*(1./allevents)) # normalization, including xsec, lumi, global fudge-factor/weight, applied in addition to 1/N
                    print "LumiWeight = lumi*xsec*normfudge*(1./allevents) : ",pset.lumi,"*",xsec,"*",normfudge,"*(1./",allevents,")"," = ",lumiweight
                elif 0.0 < pset.mcfraction and pset.mcfraction < 1.0 and pset.fraction == 1.0 : #reduce mc statistics, increase mc weight to compensate
                    lumiweight = (pset.lumi*xsec*normfudge*(1./allevents)*(1./pset.mcfraction)) # normalization, including xsec, lumi, global fudge-factor/weight, applied in addition to 1/N
                    print "LumiWeight = lumi*xsec*normfudge*(1./allevents)*(1./mcfraction) : ",pset.lumi,"*",xsec,"*",normfudge,"*(1./",allevents,")*(1./",pset.mcfraction,") = ",lumiweight,"\n"
                else : print "ERROR in mcfraction and/or fraction parameters, exiting.."; exit(1)
                lumiWeight="("+str(lumiweight)+")"
            elif not pset.hcountName and not isData : 
                lumiweight = normfudge
                lumiWeight="("+str(lumiweight)+")"
            # -------------------------------------
            # Lastly, define "Weight" variable i.e.combination of all relevant weights
            # -------------------------------------
            if   pset.useUnityWeights : globalskimtree = globalskimtree.Define(pset.weightName,"(1.0)")
            else                      : globalskimtree = globalskimtree.Define(pset.weightName,commonMCWeights+"*"+bundleWeights+"*"+nloWeight+"*"+lumiWeight+"*"+puWeight)
            print pset.weightName,": "+commonMCWeights+"*"+bundleWeights+"*"+nloWeight+"*"+lumiWeight+"*"+puWeight
            branchListToSave.push_back(pset.weightName)         
        #########
        #########

        # -------------------------------------
        # Create a new skim tree with only the relevant variables and per-event "weight"
        # -------------------------------------
        outputSkimtreeFileName = ""
        if pset.inputIndex == -1 : outputSkimtreeFileName = "InputBundle_"+bundle+"_Input_"+str(iinput)+"_Tree.root"
        else                     : outputSkimtreeFileName = "InputBundle_"+bundle+"_Input_"+str(pset.inputIndex)+"_Tree.root"
        if pset.inputdir : outputSkimtreeFileName = pset.inputdir+"/"+outputSkimtreeFileName
        else             : outputSkimtreeFileName = directoryName+"/"+outputSkimtreeFileName
        #
        if "c" in pset.mode : # skip this creation step if "c" is not specified in mode!
            #print "branchListToSave: ",branchListToSave
            globalskimtree.Snapshot(treename, outputSkimtreeFileName, branchListToSave)
            print "\nOutput file for skimmed tree: "+outputSkimtreeFileName,"\n"
            #
            outputSkimtreeFile=ROOT.TFile(outputSkimtreeFileName,"UPDATE")
            outputSkimtreeFile.cd()
            # -------------------------------------
            # Write out idenfitication information to the skim tree file
            # -------------------------------------
            inputstree = ROOT.TTree( 'inputstree', 'Tree of pset.inputs' )
            iinput_vector             = ROOT.vector('int')();      iinput_vector.push_back(iinput)
            bundle_vector             = ROOT.vector('string')();   bundle_vector.push_back(bundle)
            xsec_vector               = ROOT.vector('string')();   xsec_vector.push_back(str(xsec))
            mfilename_vector          = ROOT.vector('string')();   mfilename_vector.push_back(mfilename)
            normfudge_vector          = ROOT.vector('string')();   normfudge_vector.push_back(str(normfudge))
            relflatunc_vector         = ROOT.vector('string')();   relflatunc_vector.push_back(str(relflatunc))
            technicalselection_vector = ROOT.vector('string')();   technicalselection_vector.push_back(technicalselection)
            globalselection_vector    = ROOT.vector('string')();   globalselection_vector.push_back(pset.globalselection)
            isData_vector             = ROOT.vector('int')();      isData_vector.push_back(isData)
            inputstree.Branch( 'iinput', iinput_vector )
            inputstree.Branch( 'bundle', bundle_vector )
            inputstree.Branch( 'xsec', xsec_vector )
            inputstree.Branch( 'mfilename', mfilename_vector )
            inputstree.Branch( 'normfudge', normfudge_vector )
            inputstree.Branch( 'relflatunc', relflatunc_vector )
            inputstree.Branch( 'technicalselection', technicalselection_vector )
            inputstree.Branch( 'globalselection', globalselection_vector )
            inputstree.Branch( 'isData', isData_vector )
            inputstree.Fill()
            #print "writing inputstree1"
            inputstree.Write("PsetInputsTree")
            #print "writing inputstree2"
            #
            # -------------------------------------
            # Write out PU profile and eventfilter information to the skim tree file
            # -------------------------------------
            if pset.hpuName :
                if "/" not in pset.hpuName :
                    hpuToWrite = mfile.Get(pset.hpuName); outputSkimtreeFile.cd(); hpuToWrite.Write(pset.hpuName)
                else :
                    hpuDir = ""; hpuDirArray = pset.hpuName.split("/")[:-1];  
                    for folder in hpuDirArray : hpuDir += folder+"/"
                    hpuToWrite = mfile.Get(pset.hpuName); outputSkimtreeFile.cd(); outputSkimtreeFile.mkdir(hpuDir);  outputSkimtreeFile.cd(hpuDir);  
                    hpuToWrite.Write(pset.hpuName.split("/")[-1])
            #
            if pset.hcountName :
                if "/" not in pset.hcountName :
                    hcountToWrite = mfile.Get(pset.hcountName); outputSkimtreeFile.cd();  hcountToWrite.Write(pset.hcountName)
                else :
                    hcountDir = ""; hcountDirArray = pset.hcountName.split("/")[:-1]; 
                    for folder in hcountDirArray : hcountDir += folder+"/"
                    hcountToWrite = mfile.Get(pset.hcountName); outputSkimtreeFile.cd(); outputSkimtreeFile.mkdir(hcountDir); outputSkimtreeFile.cd(hcountDir);
                    hcountToWrite.Write(pset.hcountName.split("/")[-1])
            #

        # -------------------------------------
        # Loop over and apply local selections
        # -------------------------------------
        mfileskim=ROOT.TFile(outputSkimtreeFileName,"READ")
        trlocal=mfileskim.Get(treename)
        print trlocal.GetEntries()
        for ilocalsel,localsel in enumerate(pset.localselection):
            #
            if localsel=="1" : localsel="(1>0)"
            localselparsed = localsel
            localselparsed = localselparsed.replace("[]","_Vector_")
            localselparsed = localselparsed.replace("[","_")
            localselparsed = localselparsed.replace("]","_") 
            localselparsed = localselparsed.replace(" ","")
            for var in nonvectorbranches : 
                if var not in localselparsed : continue
                localselparsed_temp = "#"+localselparsed+"#"
                localselparsed_temp = localselparsed_temp.replace("<","#<#")
                localselparsed_temp = localselparsed_temp.replace(">","#>#")
                localselparsed_temp = localselparsed_temp.replace("=","#=#")
                localselparsed_temp = localselparsed_temp.replace("&","#&#")
                localselparsed_temp = localselparsed_temp.replace("|","#|#")
                localselparsed_temp = localselparsed_temp.replace("(","#(#")
                localselparsed_temp = localselparsed_temp.replace(")","#)#")
                localselparsed_temp = localselparsed_temp.replace("#"+var+"#","#"+var+"_0_#")
                localselparsed = localselparsed_temp.replace("#","")
            
            print "\nApplying local selection: ",localsel
            #
            trlocal.SetEntryList(0) #reset any previously applied skim 
            localskimtentrylistname="localskim"+str(iinput)+"_"+str(ilocalsel)
            localskim = ROOT.TEntryList(localskimtentrylistname,localskimtentrylistname) #declare the TEntryList with a "unique name"
            trlocal.Draw(">>"+localskimtentrylistname,localselparsed,"entrylist")
            print "\n No of events passing the global+local skim: ",localskim.GetN(),"\n"
            trlocal.SetEntryList(localskim);
            #
            #print "parsedvariables: ",parsedvariables1D
            # -------------------------------------
            # Loop over and plot variables per local selection
            # -------------------------------------
            if "p" not in pset.mode : continue
            parsedvariablesdict = dict((x, y) for x, y in parsedvariables2D)
            for ivar,(var,nbin,binlow,binhigh,xlabel,isDiscrete) in enumerate(pset.variables):
                #
                if pset.inputIndex == -1 : histoName = "h_"+str(ilocalsel)+"_localsel_"+str(iinput)+"_"+bundle+"_"+var
                else                     : histoName = "h_"+str(ilocalsel)+"_localsel_"+str(pset.inputIndex)+"_"+bundle+"_"+var
                #                
                weight    = pset.weightName
                ###
                var_ = parsedvariables1D[ivar]
                #
                #print "var/var_/parsedalias: ",var," / ",var_," / ",parsedvariablesdict[var_]
                print "Drawing TH1 for variable = ",var,"  ",parsedvariablesdict[var_]
                h_=ROOT.TH1D("h_","h_",nbin,binlow,binhigh)
                trlocal.Draw(var_+">>h_",weight)
                #
                # -------------------------------------
                # WARNING if there is underflow
                # -------------------------------------
                if h_.GetBinContent(0) != 0.0 : print "WARNING::Underflow detected: ",h_.GetBinContent(0)," in variable: ",var
                #
                # -------------------------------------
                # Add overflow to last bin
                # -------------------------------------
                print "Before h_.GetEntries(): ",h_.GetEntries()
                lastbincontent  = h_.GetBinContent(h_.GetNbinsX())
                overflowcontent = h_.GetBinContent(h_.GetNbinsX()+1)
                lastbinerror  = h_.GetBinError(h_.GetNbinsX())
                overflowerror = h_.GetBinError(h_.GetNbinsX()+1)
                h_.SetBinContent(h_.GetNbinsX(),lastbincontent+overflowcontent)
                h_.SetBinError(h_.GetNbinsX(),pow(pow(lastbinerror,2.)+pow(overflowerror,2.),0.5))
                # -------------------------------------
                # Manually set overflow to ZERO in order not to "bug" histo integrals later on in plotMaker.
                # -------------------------------------
                h_.SetBinContent(h_.GetNbinsX()+1,0.)
                h_.SetBinError(h_.GetNbinsX()+1,0.)
                print "After h_.GetEntries(): ",h_.GetEntries()
                print "h_.Integral(1,h_.GetNbinsX()): ",h_.Integral(1,h_.GetNbinsX())
                #
                # -------------------------------------
                # Set histogram title, axis labels
                # -------------------------------------
                globaltechnicallocalselection = "("+globalandtechnicalselection+")"+"&&("+localsel+")" #used in histogram title
                h_.SetTitle(globaltechnicallocalselection)
                h_.GetXaxis().SetTitle(var)
                h_.GetYaxis().SetTitle("Count")
                #
                # -------------------------------------
                # Write histogram
                # -------------------------------------
                outputHistoFile.cd()
                h_.Write(histoName)
                ###
                """
                #hunc_=TH1D("hunc_","hunc_",nbin,binlow,binhigh)
                #binnedweightsstring="1"
                #trlocal.Draw(var+">>h_","("+binnedweightsstring+")")
                #trlocal.Draw(var+">>h_","("+weightsstring+")")
                #outputR.cd()
                #h_.Write(histoName)
                #tr.Draw(var+">>hunc_","statunc2*("+localsel+")*("+binnedweightsstring+")")
                #####
                #print "localsel: ",localsel
                #print "skim1: ",var,"::",parsedvariables[ivar][0],"  localsel:",localsel
                """
                ###
                ### Alternative histo writing - but a bit slow!!!
                """
                localskimaliastree = globalskimaliastree.Filter(localsel)
                histo = localskimaliastree.Histo1D( (var,globaltechnicallocalselection+";"+var+";Count",nbin,binlow,binhigh), parsedvariables[ivar][0], weight )
                outputHistoFile.cd()
                histo.Write(histoName)
                """
                ###
        del localskim
        del trlocal

    if "p" in pset.mode :
        outputHistoFile.ls()
        outputHistoFile.Close()

    return        




