#!/usr/bin/env python
from ROOT import *
from array import *
#import setup.treeSkimmerTHn     as treeSkimmer
import setup.treeSkimmer_CreateAndProject_TDataFrame as treeSkimmer
import setup.plotMaker          as plotMaker
import setup.argParser          as args
import setup.parameterSet       as pset
import os, time
gROOT.SetBatch(True)
gROOT.ProcessLine("gErrorIgnoreLevel = 3000;")

def parseArgs(pset,args):
    pset.inputIndex             = args.inputindex
    #pset.step                   = args.step
    #pset.maxsteps               = args.maxsteps
    pset.mode                   = args.mode
    pset.analysis               = args.analysisname if len(args.analysisname)>0 else pset.analysis
    pset.inputdir               = args.inputdir
    pset.forceinputsmatch       = args.forceinputsmatch
    # ------
    return pset

def parseBundles(pset):
    # Extract bundles 
    bundles = []
    for (bundle,xsec,mfilename,normfudge,relflatunc,technicalselection,treename) in pset.inputs :
        if bundle not in bundles and bundle not in pset.removeBundles: bundles.append(bundle)
    #
    pset.bundles = bundles
    return pset

def parseInputs(pset):
    inputs = []
    for (bundle,xsec,mfilename,normfudge,relflatunc,technicalselection,treename) in pset.inputs :
        # --
        isData=False
        if "Data" in bundle or mfilename is "Data" or "MisID" in bundle : isData=True
        # --
        if mfilename not in pset.bundles and bundle not in pset.removeBundles : # this handles cross-referencing in pset.inputs
            inputs.append((bundle,xsec,mfilename,normfudge,relflatunc,technicalselection,treename,isData))
        # --
        if mfilename in pset.bundles and bundle not in pset.removeBundles: # this handles cross-referencing in pset.inputs
            for inraw in pset.inputs :
                #if mfilename == inraw[0] : inputs.append((bundle,inraw[1],inraw[2],inraw[3],inraw[4],technicalselection,treename,isData))
                if mfilename == inraw[0] : inputs.append((bundle,xsec,inraw[2],normfudge,relflatunc,technicalselection,treename,isData))
    #
    pset.inputs = inputs
    return pset

def parseInputsFromDirectory(pset):
    #
    if pset.inputdir == "" or pset.mode == "d" : return pset
    #
    #mfile=ROOT.TFile(mfilename,"READ")
    #tr = mfile.Get(treename)
    #
    inputs = []
    cwd = os.getcwd()
    directoryinputs = [] 
    directoryinputsbundlesiinputs = []
    for treefile in os.listdir(pset.inputdir) :
        if "Tree.root" not in treefile: continue
        pathtotreefile = cwd+"/"+pset.inputdir+"/"+treefile
        print pathtotreefile
        directoryinputs.append(pathtotreefile)
        #runPlotter_Test_inputBundle_WW2_Input_1_Tree.root
        dirinputiinput = str((treefile.replace("_Tree.root","").split("_"))[-1])
        dirinputbundle = str((treefile.replace("_Tree.root","").split("_"))[-3])
        directoryinputsbundlesiinputs.append(dirinputbundle+"_"+dirinputiinput)
    directoryinputsdict = dict(zip(directoryinputsbundlesiinputs,directoryinputs))
    #
    # --
    for iinput, (bundle,xsec,mfilename,normfudge,relflatunc,technicalselection,treename,isData) in enumerate(pset.inputs):
        key = bundle+"_"+str(iinput)
        # -------------------------------------
        # Check if specified bundle in pset.inputs actually exist in the provided input directory for fast processing
        # -------------------------------------
        if key not in directoryinputsdict :  print "ERROR :: Input directory doesnt have all the files specified in pset.inputs, exiting!!"; exit(1)
        #
        # -------------------------------------
        # Get the skim input tree for the desired input from the provided input directory, check if it indeed has the right global/technical selection etc!
        # -------------------------------------
        mfilenamefrominputdir = directoryinputsdict[key] # call input directory counterpart using bundle+iinput "key"
        psetinputstreefile = TFile(mfilenamefrominputdir,"READ")
        psetinputstree = psetinputstreefile.Get("PsetInputsTree")
        psetinputstree.GetEntry(0)
        hasinputsmismatch = False
        if pset.globalselection != psetinputstree.globalselection[0]    : hasinputsmismatch = True
        if technicalselection   != psetinputstree.technicalselection[0] : hasinputsmismatch = True
        if str(xsec)            != psetinputstree.xsec[0]               : hasinputsmismatch = True
        if str(normfudge)       != psetinputstree.normfudge[0]          : hasinputsmismatch = True
        if str(relflatunc)      != psetinputstree.relflatunc[0]         : hasinputsmismatch = True
        if int(isData)          != psetinputstree.isData[0]             : hasinputsmismatch = True
        if hasinputsmismatch and pset.forceinputsmatch : 
            print "\nERROR :: Skim tree in input directory was generated with different pset.inputs!"
            print "Here is the comparison for input bundle: ",bundle," iinput: ",iinput
            print "                               filename: ",mfilename
            print "---"
            print "             pset.globalselection: ",pset.globalselection
            print "psetinputstree.globalselection[0]: ",psetinputstree.globalselection[0]
            print "---"
            print "      pset.inputs.technicalselection: ",technicalselection
            print "psetinputstree.technicalselection[0]: ",psetinputstree.technicalselection[0]
            print "---"
            print "      pset.inputs.xsec: ",xsec
            print "psetinputstree.xsec[0]: ",psetinputstree.xsec[0]
            print "---"
            print "      pset.inputs.normfudge: ",normfudge
            print "psetinputstree.normfudge[0]: ",psetinputstree.normfudge[0]
            print "---"
            print "      pset.inputs.relflatunc: ",relflatunc
            print "psetinputstree.relflatunc[0]: ",psetinputstree.relflatunc[0]
            print "---"
            print "      pset.inputs.isData: ",int(isData)
            print "psetinputstree.isData[0]: ",psetinputstree.isData[0]
            print "---"
            print "    ..exiting!"
            exit(1)
        else :inputs.append((bundle,xsec,mfilenamefrominputdir,normfudge,relflatunc,technicalselection,treename,isData))
    # --
    pset.inputs = inputs
    return pset

def runPlotter(pset,args):
    #
    # Parse the args, bundles, inputs, modify pset accordingly:
    pset = parseArgs(pset,args)
    pset = parseBundles(pset)
    #print "STEP 0 ::::::: ",pset.inputs
    pset = parseInputs(pset)
    #print "STEP 1 ::::::: ",pset.inputs
    pset = parseInputsFromDirectory(pset) # this needs to be called after "parseInputs()" in order to correctly handle cross-referencing in pset.inputs
    #print "STEP 2 ::::::: ",pset.inputs
    if pset.inputIndex > -1 : pset.inputs = [pset.inputs[pset.inputIndex]] # Trim the inputs by the specified index, to be used for parallel processing of each input
    #print "STEP 3 ::::::: ",pset.inputs
    #
    #
    #exit(0)
    #
    if not args.execute :
        return pset
    else :
        print "\nrunPlotter.py BEGIN\n"
        print "pset.mode: ",pset.mode
        # Check input parameter set, make sure all needed parameters are defined
        pset.checkPSet(pset,args.stamp)
        #
        directoryName = "runPlotter_"+pset.analysis
        timestr    = time.strftime("%Y%m%d%H%M%S")
        #
        if "c" in pset.mode : os.system("mv     "+directoryName+"/runPlotter*.pdf "+directoryName+"/archive"+timestr+".pdf")
        if "c" in pset.mode : os.system("rm -rf "+directoryName+"/*.root")   
        if "c" in pset.mode : os.system("rm -rf "+directoryName+"/LocalSelection*")            
        #    
        if "p" in pset.mode : os.system("rm -rf "+directoryName+"/*TH1*.root")   
        if "p" in pset.mode : os.system("rm -rf "+directoryName+"/LocalSelection*")            
        # Run the treeSkimmer: produces a root file for all the inputs[] and variables[], normalized as specified above.
        if "c" in pset.mode or "p" in pset.mode :
            ####treeSkimmer.treeSkimmer(pset)
            treeSkimmer.treeSkimmer_CreateAndProject(pset)
        # Run the plotMaker: produces plots (pdf) using the root file from treeSkimmer.
        if "d" in pset.mode :
            print "plotMaker.plotMaker"
            plotMaker.plotMaker(pset)
            print "\nrunPlotter.py END\n"
            os._exit(0)
        #----
    print "\nrunPlotter.py END\n"
    os._exit(0)
