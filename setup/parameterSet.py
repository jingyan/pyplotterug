#!/usr/bin/env python
import sys
import tools.Aliases as Aliases
# ---------------------------------------------------------------------------------
## # User defined mandatory parameters
analysis=""
globalselection=""
localselection=""
#setAliases=""
variables=""
mode=""
lumi=""
year=""
inputs=[]
inputdirectory=""
useCustomXlabels=""
showSelection=""
usePoissonErrorBarsOnData=""
doRatio=""
doZscore=""
useUnityWeights=""
usePublicationStyle=""
toolsAliases = Aliases
aliases = []
branchesToSave = []
branchesToSaveDataOnly = []
branchesToSaveMCOnly = []
## # Optional parameters
bundleWeights=""
commonMCWeights=""
bundleColors=""
overlayBundles=""
removeBundles=""
#setSystematicsAliases=False
plotStatSystUnc=False
plotUncUpperPanel=False
fraction = 1.0
mcfraction = 1.0
useOverlayForRatio=False
ratioLabel = "Obs/Exp"
templateReferenceLS = -1
templateBundle = ""
smoothBundle = ""
templateVarContains = ""
smoothVarContains = ""
## # Private variables
tr              = "rootTupleTreeVeryLoose/tree"
hcountName      = "dileptonEventFilter/EventCount/EventCounter"
hpuName         = "dileptonEventFilter/TrueNumInteractionsBX0/PileUpProfile"
gentruepuweight = "GenPileUpInteractionsTrue" # Branch name
nloweightName   = "GenWeight" # Branch name
pumode          = "none" # options are: nominal, up, down, ...
weightName      = "privateWeight"
bundles         = []
isDiscrete      = True
isContinuous    = False

# ---------------------------------------------------------------------------------

def checkPSet(pset,stamp=""):
    #
    if len(stamp)>0 : pset.analysis = pset.analysis+"_"+str(stamp)
    #
    if len(pset.bundleWeights)==0 : # tuple needs to be checked!
        print "\n  WARNING:: Unspecified optional input parameter: bundleWeights"
        print "          Example input:   bundleWeights=[(\"DY\",\"jetWeights_DY(NGOODJETS[0])\"),"
        print "                                          (\"WZ\",\"jetWeights_WZ(NGOODJETS[0])\")]"
        print "          Parameters: processName, weightFunction"
        print "          Custom functions need to be defined in the tools folder and loaded to the runPlotter macro.\n"
    #
    if len(pset.bundleColors)==0 : # tuple needs to be checked!
        print "\n  WARNING:: Unspecified optional input parameter: bundleColors"
        print "          Example input:   bundleColors=[\"Data==1\",\"WW==kGreen+1\",\"WZ==kOrange+7\",\"ZZ==kMagenta-2\",\"DY==kRed+1\",\"ttbar==kBlue\"]"
        print "          Parameters: (bundleName,bundleColor)"
        print "          The custom bundle colors are applied only if all defined bundles are accounted for. Otherwise, the default configuration is used."
        print "    INFO::  Root colors: (https://root.cern.ch/doc/master/pict1_TColor_002.png)"
        print "              kWhite =0,   kBlack =1,   kGray=920,"
        print "              kRed   =632, kGreen =416, kBlue=600, kYellow=400, kMagenta=616, kCyan=432,"
        print "              kOrange=800, kSpring=820, kTeal=840, kAzure =860, kViolet =880, kPink=900.\n"
    #
    if len(pset.overlayBundles)==0 : # tuple needs to be checked!
        print "\n  WARNING:: Unspecified optional input parameter: overlayBundles"
        print "          Example input:   overlayBundles=[\"Seesaw660\",\"SeeSaw440\"]"
        print "          Parameters: bundleName"
        print "          Bundles included in the overlayBundles are drawn in the \"Overlay\" mode.\n"
    #
    if len(pset.removeBundles)==0 : # tuple needs to be checked!
        print "\n  WARNING:: Unspecified optional input parameter: removeBundles"
        print "          Example input:   removeBundles=[\"Seesaw660\",\"SeeSaw440\"]"
        print "          Parameters: bundleName"
        print "          Bundles included in the removeBundles are excluded in the histograms! \n"
    #
    if len(pset.commonMCWeights)==0 : # tuple needs to be checked!
        print "\n  WARNING:: Unspecified optional input parameter: commonMCWeights"
        print "          Example input:   commonMCWeights=[\"pileupWeight(VertexN[0])\"]"
        print "          Parameters: weightFunction(arguments)"
        print "          Custom functions need to be defined in the tools folder and loaded to the runPlotter macro.\n"
    #
    #if pset.setSystematicsAliases==True :
    #    print "\n  WARNING:: Optional input parameter in-use: setSystematicsAliases"
    #    print "          Example input:   setSystematicsAliases=False"
    #    print "          Do not use this unless you are doing systematics studies!"
    #    print "          See alias definitions to be applied to TTree here: tools/Aliases.py\n"
    #
    if pset.plotStatSystUnc==True :
        print "\n  WARNING:: Optional input parameter in-use: plotStatSystUnc"
        print "          Example input:   plotStatSystUnc=False"
        print "          This adds the systematic uncertainties to the plots!\n"
    #
    if pset.plotUncUpperPanel==True :
        print "\n  WARNING:: Optional input parameter in-use: plotUncUpperPanel"
        print "          Example input:   plotUncUpperPanel=False"
        print "          This moves the total background uncertainty to the upper panel.\n"
    #
    if pset.fraction!=1.0 :
        print "\n  WARNING:: Optional input parameter in-use: fraction"
        print "          Example input:   fraction=1.0"
        print "          This forces to only include a fraction of DATA and MC (effectively reducing luminosity)! Use only for debugging!\n"
    #
    if pset.mcfraction!=1.0 :
        print "\n  WARNING:: Optional input parameter in-use: mcfraction"
        print "          Example input:   mcfraction=1.0"
        print "          This forces to only include a fraction of the MC! Use only for debugging!\n"
    #
    if pset.useOverlayForRatio==True :
        print "\n  WARNING:: Optional input parameter in-use: useOverlayForRatio"
        print "          Example input:   useOverlayForRatio=False"
        print "          This uses the sum of overlay histograms in obs/exp ratio and in ratio panel (so only use when there is a single overlay histogram for this to be meaningful).\n"
    #
    if str(ratioLabel) != "Obs/Exp" :
        print "\n  WARNING:: Optional input parameter in-use: ratioLabel"
        print "          Example input:   ratioLabel=\"Obs/Exp\""
        print "          This sets the y-axis label of ratio panel.\n"
    #
    #
    #
    # ----------
    #
    isMissing=False
    #
    if pset.analysis=="" or not isinstance(pset.analysis,str) :
        print "\n  ERROR:: Missing input parameter: analysis"
        print "          Example input:  analysis=\"DiMuonAnalysis\""
        isMissing=True
    #
    if pset.globalselection=="" or not isinstance(pset.analysis,str) :
        print "\n  ERROR:: Missing input parameter: globalselection"
        print "          Example input:   globalselection=\"NGOODMUONS==2&&PTGOODMUONS[0]>25&&PTGOODMUONS[1]>15\""
        isMissing=True
    #
    if len(pset.localselection)==0 : # tuple needs to be checked!
        print "\n  ERROR:: Missing input parameter: localselection"
        print "          Example input:   localselection=[\"1\","
        print "                                           \"HT>200&&NLEPTONS==2\","
        print "                                           \"HT<200&&NLEPTONS==2\"]"
        print "          Only use the following (in)equality relations: < , > , ==    (== is for discrete only)."
        print "          localselection=\"1\" is the **no-local-selection** configuration."
        isMissing=True
    #
    #if pset.setAliases=="" or not isinstance(pset.setAliases,bool) :
    #    print "\n  ERROR:: Missing input parameter: setAliases"
    #    print "          Example input:   setAliases=True"
    #    print "          See alias definitions to be applied to TTree here: tools/Aliases.py"
    #    isMissing=True
    #
    #if pset.setSystematicsAliases=="" or not isinstance(pset.setSystematicsAliases,bool) :
    #    print "\n  ERROR:: Missing (optional) input parameter: setSystematicsAliases"
    #    print "          Example input:   setSystematicsAliases=False"
    #    print "          Do not use this unless you are doing systematics studies!"
    #    print "          See alias definitions to be applied to TTree here: tools/Aliases.py"
    #    isMissing=True
    #
    if pset.plotStatSystUnc=="" or not isinstance(pset.plotStatSystUnc,bool) :
        print "\n  ERROR:: Missing (optional) input parameter: plotStatSystUnc"
        print "          Example input:   plotStatSystUnc=False"
        print "          This adds the systematic uncertainties to the plots!\n"
        isMissing=True
    #
    if pset.plotUncUpperPanel=="" or not isinstance(pset.plotUncUpperPanel,bool) :
        print "\n  ERROR:: Missing (optional) input parameter: plotUncUpperPanel"
        print "          Example input:   plotUncUpperPanel=False"
        print "          This moves the total background uncertainty to the upper panel.\n"
        isMissing=True
    #
    if pset.useOverlayForRatio=="" or not isinstance(pset.useOverlayForRatio,bool) :
        print "\n  ERROR:: Missing (optional) input parameter: useOverlayForRatio"
        print "          Example input:   useOverlayForRatio=False"
        print "          This uses the sum of overlay histograms in obs/exp ratio and in ratio panel (so only use when there is a single overlay histogram for this to be meaningful).\n"
        isMissing=True
    #
    if len(str(pset.ratioLabel)) == 0 :
        print "\n  ERROR:: Missing or invalid (optional) input parameter in-use: ratioLabel"
        print "          Example input:   ratioLabel=\"Obs/Exp\""
        print "          This sets the y-axis label of ratio panel.\n"
        isMissing=True
    #
    if len(pset.variables)==0 : # tuple needs to be checked!
        print "\n  ERROR:: Missing input parameter: variables"
        print "          Example input:   variables=[(\"NGOODMUONS[0]\",6,-0.5,5.5,\"No. of Muons\",True)]"
        print "          Parameters: variableName, nBins, xLow, xHigh, xLabel, isDiscrete"
        isMissing=True
    #
    if pset.lumi=="" or not isinstance(pset.lumi,float) :
        print "\n  ERROR:: Missing or non-float type input parameter: lumi"
        print "          Example input:   lumi=1000."
        print "          Luminosity is to be expressed in /pb "
        isMissing=True
    #
    if len(pset.inputs)==0 : # tuple needs to be checked!
        print "\n  ERROR:: Missing input parameter: inputs"
        print "          Example input:   inputs=[(\"Data\",1,\"/path/to/file.root\",1,0),"
        print "                                   (\"X\",3,\"/path/to/file.root\",1.1,0.2)]"
        print "          Parameters: processName, Xsec (pb), path-to-file, NormalizationFudge/Weight, RelativeFlatUncertainty"
        print "          Data is a special keyword for process name, everything else is user-defined."
        print "          Xsec, NormFudge/Weight, and RelFlatUnc parameters are dummy values for Data. They are **not** applied."
        print "          Files are listed here: https://twiki.cern.ch/twiki/bin/viewauth/CMS/RutgersMultileptonSampleProcessing"
        isMissing=True
    #
    if pset.useCustomXlabels=="" or not isinstance(pset.useCustomXlabels,bool) :
        print "\n  ERROR:: Missing input parameter: useCustomXlabels"
        print "          Example input:   useCustomXlabels=True"
        print "          This prints the xLabel as specified in variables input as the title of the histogram x-axis."
        isMissing=True
    #
    if pset.showSelection=="" or not isinstance(pset.showSelection,bool) :
        print "\n  ERROR:: Missing input parameter: showSelection"
        print "          Example input:   showSelection=True"
        print "          This prints the applied global and local selection as the histogram title."
        isMissing=True
    #
    if pset.usePoissonErrorBarsOnData=="" or not isinstance(pset.usePoissonErrorBarsOnData,bool) :
        print "\n  ERROR:: Missing input parameter: usePoissonErrorBarsOnData"
        print "          Example input:   usePoissonErrorBarsOnData=False"
        print "          Please see https://twiki.cern.ch/twiki/bin/viewauth/CMS/PoissonErrorBars for details."
        isMissing=True
    #
    if pset.doRatio=="" or not isinstance(pset.doRatio,bool) :
        print "\n  ERROR:: Missing input parameter: doRatio"
        print "          Example input:   doRatio=True"
        isMissing=True
    #
    if pset.doZscore=="" or not isinstance(pset.doZscore,bool) :
        print "\n  ERROR:: Missing input parameter: doZscore"
        print "          Example input:   doZscore=False"
        print "          This is an option to compute per-bin (local) z-scores (derived from p-values)."
        print "          Statistical and systematic uncertainties on expected (background) contributions are taken into account."
        print "          This option works only when doRatio is enabled."
        print "          This option works only when observed (Data) and expected contributions are present."
        isMissing=True
    #
    if pset.useUnityWeights=="" or not isinstance(pset.useUnityWeights,bool) :
        print "\n  ERROR:: Missing input parameter: useUnityWeights"
        print "          Example input:   useUnityWeights=False"
        print "          This option force-resets process and event weights to unity (1) everywhere."
        isMissing=True
    #
    if pset.usePublicationStyle=="" or not isinstance(pset.usePublicationStyle,bool) :
        print "\n  ERROR:: Missing input parameter: usePublicationStyle"
        print "          Example input:   usePublicationStyle=False"
        print "          This option enables publication style labels on plots."
        isMissing=True
    #
    if "Data" in overlayBundles : # tuple needs to be checked!
        print "\n  ERROR:: \"Data\" bundle cannot be an input parameter for overlayBundles; please use other bundles."
        print "          Example input:   overlayBundles=[\"Seesaw660\",\"SeeSaw440\"]"
        print "          Parameters: bundleName"
        print "          Bundles included in the overlayBundles are drawn in the \"Overlay\" mode.\n"
        isMissing=True
    #
    #if pset.step>pset.maxsteps or pset.step<0 or not isinstance(pset.step,int) :
    #    print "\n  ERROR:: Optional input parameter has invalid value: step"
    #    print "          Example input:   step=1"
    #    print "          This forces to only include a k/maxsteps slice of DATA and MC but doesnt reduce the lumi! Use only for parallel processing!\n"
    #    print "          Set step to 0 to disable, 1-100 to select the k^th 1/maxsteps slice of DATA and MC."
    #    print "          Total number of steps i.e. maxsteps cannot be more than 100 or less than 1."
    #    isMissing=True
    #
    #if pset.maxsteps>100 or pset.maxsteps<1 or not isinstance(pset.maxsteps,int) :
    #    print "\n  ERROR:: Optional input parameter has invalid value: maxsteps"
    #    print "          Example input:   maxsteps=1"
    #    print "          This forces to split DATA and MC into maxsteps slices but doesnt reduce the lumi! Use only for parallel processing!\n"
    #    print "          Set maxstep to 1 to disable, 2-100 to split DATA and MC into maxsteps number of slices."
    #    print "          Total number of steps i.e. maxsteps cannot be more than 100 or less than 1."
    #    isMissing=True
    #
    if pset.fraction>1.0 or pset.fraction<0.0 or not isinstance(pset.fraction,float) :
        print "\n  ERROR:: Optional input parameter has invalid value: fraction"
        print "          Example input:   fraction=1.0"
        print "          This forces to only include a fraction of DATA and MC (effectively reducing luminosity)! Use only for debugging!\n"
        isMissing=True
    #
    if pset.mcfraction>1.0 or pset.mcfraction<0.0 or not isinstance(pset.mcfraction,float) :
        print "\n  ERROR:: Optional input parameter has invalid value: mcfraction"
        print "          Example input:   mcfraction=1.0"
        print "          This forces to only include a fraction of MC! Use only for debugging!\n"
        isMissing=True
    #
    if pset.mcfraction!=1.0 and pset.fraction!=1.0 :
        print "\n  ERROR:: Conflicting optional input parameters have been used: mcfraction and fraction!"
        print "          Example input:   mcfraction=1.0 and  fraction=1.0"
        isMissing=True
    #
    #if pset.step>0 and pset.mcfraction!=1.0 :
    #    print "\n  ERROR:: Conflicting optional input parameters have been used: step and mcfraction!"
    #    print "            Can't use both step and mcfraction" 
    #    isMissing=True
    #
    #if pset.step>1 and pset.fraction!=1.0 :
    #    print "\n  ERROR:: Conflicting optional input parameters have been used: step and fraction!"
    #    print "            Can't use both step and fraction" 
    #    isMissing=True
    #
    if pset.mode != "c" and pset.mode != "p" and pset.mode != "d" and pset.mode != "cp" and pset.mode != "pd" and pset.mode != "cpd" : 
        print "\n  ERROR:: Valid mode value expected (create/project/draw): c/p/d/cp/pd/cpd !"
        isMissing=True
    #
    #
    if isMissing : print "\n  ERROR:: Exiting..\n"; sys.exit(1)
    
# ---------------------------------------------------------------------------------

