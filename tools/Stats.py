#!/usr/bin/env python
from ROOT import *
gROOT.SetBatch(True)

def pValue(Obs, Exp, ExpError, debug=False): # ExpError is relative uncertainty on Exp.
    doPlots=False
    # Protection against unphysical quantities
    if Exp<=0     : return -99
    if ExpError<0 : return -99
    if Obs<0      : return -99
    #
    if debug : print "\npValue( Obs: ",Obs," Exp: ",Exp," ExpError: ",ExpError," )"
    #
    if debug and Obs<Exp  : 
        # Cumulative distribution function of the Poisson distribution: "Lower tail" of the integral of the poisson_pdf (including 1/2 of the Obs bin probability).
        if Obs==0 : print "No Sys.Unc. pvalue: ",Math.poisson_pdf(int(Obs),Exp)*0.5 
        else      : print "No Sys.Unc. pvalue: ",Math.poisson_cdf(int(Obs-1),Exp)+Math.poisson_pdf(int(Obs),Exp)*0.5
    #
    if debug and Obs>=Exp : 
        # "Upper tail" of the integral of the poisson_pdf (including 1/2 of the Obs bin probability).
        print "No Sys.Unc. pvalue: ",Math.poisson_cdf_c(int(Obs),Exp)+Math.poisson_pdf(int(Obs),Exp)*0.5 
    #
    if ExpError<0.001 : # if less than 0.1% relative error on Exp, do not do any numeric calculation, but used built-in ROOT functions instead.
        if  Obs<Exp : 
            if Obs==0 : return Math.poisson_pdf(int(Obs),Exp)*0.5
            else      : return Math.poisson_cdf(int(Obs-1),Exp)+Math.poisson_pdf(int(Obs),Exp)*0.5
            #####return Math.poisson_cdf(int(Obs),Exp)
        else        : 
            #####return Math.poisson_cdf_c(int(Obs),Exp)+Math.poisson_pdf(int(Obs),Exp)
            return Math.poisson_cdf_c(int(Obs),Exp)+Math.poisson_pdf(int(Obs),Exp)*0.5
    #
    nbins=int(max(max(Obs,Exp)*100,100)+1)
    xhigh=nbins-0.5
    if doPlots : print "Histogram nBins: ",nbins," Range: ",-0.5," to ",xhigh
    ExpPDFnoSystematic   = TH1D("ExpPDFnoSystematic",  "",nbins,-0.5,xhigh)
    ExpPDFGaussError     = TH1D("ExpPDFGaussError",    "",nbins,-0.5,xhigh)
    ExpPDFLogNormalError = TH1D("ExpPDFLogNormalError","",nbins,-0.5,xhigh)
    logNormal=TF1("logNormal","(1.0/(sqrt(2.*TMath::Pi())*x*TMath::Log([1])))*TMath::Exp(-TMath::Power(TMath::Log(x)/[0],2)/(2*TMath::Power(TMath::Log([1]),2)))",0,max(ExpError*10,5))
    logNormal.SetParameter(1,1+ExpError) # uncertainty
    logNormal.SetParameter(0,1) # mean
    #
    pValueFromExpPDFwLogNormalError=0
    nToys=100000.
    for i in range(int(nToys)):
        r = TRandom3(); r.SetSeed(0)
        #
        ExpPDFnoSystematic.Fill(r.PoissonD(Exp))
        #
        if doPlots : poisMuGaussError=r.Gaus(Exp,ExpError*Exp) # this is "gaussian smeared" poisson mu
        if doPlots : itoyGaussError=r.PoissonD(poisMuGaussError) 
        #
        poisMuLogNormalError=logNormal.GetRandom()*Exp # this is "logNormal smeared" poisson mu
        itoyLogNormalError=r.PoissonD(poisMuLogNormalError)
        #
        if (Obs < Exp and itoyLogNormalError<=Obs) or (Obs>=Exp and itoyLogNormalError>=Obs) : 
            weight=1.
            if itoyLogNormalError==Obs : weight=0.5 # including 1/2 of the Obs bin probability
            pValueFromExpPDFwLogNormalError+=(weight/nToys) # sum up toys if as extreme or more as the observed (p-value)
        #
        if doPlots and itoyGaussError>xhigh     : itoyGaussError=xhigh-0.1 # silly overflow protection for hisogram
        if doPlots and itoyLogNormalError>xhigh : itoyLogNormalError=xhigh-0.1 # silly overflow protection for hisogram
        if doPlots : ExpPDFGaussError.Fill(itoyGaussError)
        if doPlots : ExpPDFLogNormalError.Fill(itoyLogNormalError)
    #
    if doPlots :
        can1=TCanvas("can1","can1",0,0,600,600)
        can1.SetLogy(1)
        leg = TLegend(0.35,0.7,0.86,0.89); 
        leg.SetFillColor(0); leg.SetLineColor(0); leg.SetFillColor(0); leg.SetTextSize(0.03); leg.SetTextFont(42);
        #
        ExpPDFnoSystematic.Scale(1./nToys)
        ExpPDFGaussError.Scale(1./nToys)
        ExpPDFLogNormalError.Scale(1./nToys)
        #
        gStyle.SetOptStat(0)
        ExpPDFnoSystematic.GetYaxis().SetRangeUser(0.0001,10)
        ExpPDFnoSystematic.SetFillColor(kGray+1)
        ExpPDFnoSystematic.SetLineColor(kGray+1)
        ExpPDFnoSystematic.SetFillStyle(1001)
        ExpPDFnoSystematic.Draw("HIST")
        ExpPDFnoSystematic.GetYaxis().SetTitle("PDF")
        ExpPDFnoSystematic.GetYaxis().SetTitleOffset(1.2)
        ExpPDFnoSystematic.GetXaxis().SetTitle("X")
        ExpPDFnoSystematic.GetXaxis().SetRangeUser(-0.5,int(Exp*4)+3.5)
        ExpPDFGaussError.SetLineColor(2)
        ExpPDFGaussError.SetLineWidth(2)
        ExpPDFGaussError.SetFillStyle(0)
        ExpPDFGaussError.Draw("HISTsame")
        ExpPDFLogNormalError.SetLineColor(4)
        ExpPDFLogNormalError.SetLineWidth(2)
        ExpPDFLogNormalError.SetFillStyle(0)
        ExpPDFLogNormalError.Draw("HISTsame")#Exp, ExpError
        leg.AddEntry(ExpPDFnoSystematic,"Raw Poisson (mu="+str(Exp)+")","F")
        leg.AddEntry(ExpPDFGaussError,"with Gaus Sys.Unc ("+str(ExpError*100)+"%)","L")
        leg.AddEntry(ExpPDFLogNormalError,"with LogNormal Sys.Unc ("+str(ExpError*100)+"%)","L")
        leg.Draw()
        #
        can1.SetGridx(1)
        gPad.RedrawAxis()
        gPad.RedrawAxis("g")
        can1.SaveAs("Stats.pdf(")
        #
        frame=TH1D("","",10000,0,10000)
        frame.GetXaxis().SetRangeUser(0,3)
        frame.GetYaxis().SetRangeUser(0,5)
        can1.SetLogy(0)
        frame.GetYaxis().SetTitle("LogNormal(x,Mean=1,RelSigma="+str(ExpError)+")")
        frame.GetYaxis().SetTitleOffset(1.2)
        frame.GetXaxis().SetTitle("x")
        frame.Draw("HIST")
        logNormal.Draw("Lsame")
        can1.SaveAs("Stats.pdf)")
    #
    #if Obs < Exp : return ExpPDFLogNormalError.Integral(0,ExpPDFLogNormalError.FindBin(Obs))
    #else :         return ExpPDFLogNormalError.Integral(ExpPDFLogNormalError.FindBin(Obs),nbins+1)
    #
    if debug : print "pvalue with Sys.Unc (LogNormal) : ",pValueFromExpPDFwLogNormalError
    return pValueFromExpPDFwLogNormalError



def zScoreSimple(Obs,Exp,pVal,debug=False):
    # Protection against unphysical quantities
    if Exp<=0     : return -99
    if pVal<=0    : return -99
    if pVal>=1    : return -99
    if Obs<0      : return -99
    #
    if debug: print "zScoreSimple input pValue: ",pVal
    #
    if Obs>Exp : pVal=1-pVal
    zsc=TMath.NormQuantile(pVal)
    #
    if debug: print "zScore: ",zsc
    #
    return zsc

def zScore(Obs,Exp,ExpError,debug=False):
    # Protection against unphysical quantities
    if Exp<=0     : return -99
    if ExpError<0 : return -99
    if Obs<0      : return -99
    #
    pVal=pValue(Obs,Exp,ExpError,debug)
    #
    return zScoreSimple(Obs,Exp,pVal,debug)


# ------------------------------------------------------------------------------------------------------------------------
# For info on quantiles, see: 
#                              https://en.wikipedia.org/wiki/Standard_score#/media/File:Normal_distribution_and_scales.gif 
# For info on lognormal, see: 
#                              https://cds.cern.ch/record/1375842/files/ATL-PHYS-PUB-2011-011.pdf
# ------------------------------------------------------------------------------------------------------------------------


# ----------------------------------------
# Test..
"""
obs = 10200
obs =  9800
exp = 10000
#obs=5
#exp=1
expErr=0
print "Obs / Exp : ", obs," / ",exp
pvalue= pValue(obs,exp,expErr,False)
print "pvalue: ",pvalue
print "zScore: ",zScoreSimple(obs,exp,pvalue)
"""
# ----------------------------------------
