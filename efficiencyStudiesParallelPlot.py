#!/usr/bin/env python
from ROOT import *
from array import *
import setup.plotSettings as plotSettings
gROOT.SetBatch(True)
import tools.Aliases as aliases
gROOT.ProcessLineSync('.L tools/customFunctions.C+')


#inputfilename=
inputs=[ 
# ("runPlotter_UpgradeMatchingEfficiencyJune13/InputBundle_Ext_Input_0_Tree.root","ExtPU0")
#,("runPlotter_UpgradeMatchingEfficiencyJune13/InputBundle_Nom_Input_1_Tree.root","NomPU0")
# ("runPlotter_UpgradeMatchingEfficiencyJune13/InputBundle_Nom_Input_1_Tree.root","NomPU0")
 ("runPlotter_UpgradeMatchingEfficiencyJune20/InputBundle_Ext_Input_0_Tree.root","NomPU0")
]

#studyName = "June13"
studyName = "June20"

can=plotSettings.CreateCanvas1D("can")
can.cd()

track="L1Track"
globalselection = "1"



variables = [
 ("GenParticlePt","25,0,100","abs(GenParticleEta)<2.4")
,("GenParticlePt","25,0,100","abs(GenParticleEta)<2.4&&abs(GenParticleDRmin"+track+")<0.3")
,("Ratio","","")
,("GenParticlePt","25,0,100","abs(GenParticleEta)<1.0")
,("GenParticlePt","25,0,100","abs(GenParticleEta)<1.0&&abs(GenParticleDRmin"+track+")<0.3")
,("Ratio","","")
,("GenParticlePt","25,0,100","abs(GenParticleEta)>1.0&&abs(GenParticleEta)<2.4")
,("GenParticlePt","25,0,100","abs(GenParticleEta)>1.0&&abs(GenParticleEta)<2.4&&abs(GenParticleDRmin"+track+")<0.3")
,("Ratio","","")
,("GenParticleEta","25,-2.5,2.5","GenParticlePt>10")
,("GenParticleEta","25,-2.5,2.5","GenParticlePt>10&&abs(GenParticleDRmin"+track+")<0.3")
,("Ratio","","")
,("GenParticleEta","25,-2.5,2.5","GenParticlePt>20")
,("GenParticleEta","25,-2.5,2.5","GenParticlePt>20&&abs(GenParticleDRmin"+track+")<0.3")
,("Ratio","","")
,("EcalPt","25,0,100","abs(EcalEta)<1.0")
,("EcalPt","25,0,100","abs(EcalEta)<1.0&&EcalTrackDRMin<0.3")
,("Ratio","","")
,("EcalPt","25,0,100","abs(EcalEta)<1.6")
,("EcalPt","25,0,100","abs(EcalEta)<1.6&&EcalTrackDRMin<0.3")
,("Ratio","","")
,("EcalPt","25,0,100","abs(EcalEta)>1.6&&abs(EcalEta)<2.5")
,("EcalPt","25,0,100","abs(EcalEta)>1.6&&abs(EcalEta)<2.5&&EcalTrackDRMin<0.3")
,("Ratio","","")
]
variables = [
 ("EcalPt","20,0,100","abs(EcalEta)<1.0")
,("EcalPt","20,0,100","abs(EcalEta)<1.0&&EcalTrackDRMin<0.3")
,("Ratio","","")
,("EcalPt","20,0,100","abs(EcalEta)<1.0")
,("EcalPt","20,0,100","abs(EcalEta)<1.0&&EcalTrackDRMin<0.1")
,("Ratio","","")
,("EcalPt","20,0,100","abs(EcalEta)<2.4")
,("EcalPt","20,0,100","abs(EcalEta)<2.4&&EcalTrackDRMin<0.1")
,("Ratio","","")
,("EcalPt","20,0,100","abs(EcalEta)<1.0")
,("EcalPt","20,0,100","abs(EcalEta)<1.0&&abs(EcalTrackDPhiAtDR0p3MatchPtCorr)<0.03&&abs(EcalTrackDZAtDR0p3MatchPtCorr)<2.5")
,("Ratio","","")
,("EcalEta","25,-2.5,2.5","EcalPt>10")
,("EcalEta","25,-2.5,2.5","EcalPt>10&&EcalTrackDRMin<0.3")
,("Ratio","","")
,("EcalEta","25,-2.5,2.5","EcalPt>10")
,("EcalEta","25,-2.5,2.5","EcalPt>10&&EcalTrackDRMin<0.1")
,("Ratio","","")
,("EcalEta","25,-2.5,2.5","EcalPt>10&&abs(EcalEta)<1.0")
,("EcalEta","25,-2.5,2.5","EcalPt>10&&abs(EcalEta)<1.0&&abs(EcalTrackDPhiAtDR0p3MatchPtCorr)<0.03&&abs(EcalTrackDZAtDR0p3MatchPtCorr)<2.5")
,("Ratio","","")
#,("EcalEta","25,-2.5,2.5","EcalPt>10&&abs(EcalEta)<1.0")
#,("EcalEta","25,-2.5,2.5","EcalPt>10&&abs(EcalEta)<1.0&&abs(EcalTrackDPhiAtDR0p3MatchPtCorr)<0.03&&abs(EcalTrackDZAtDR0p3MatchPtCorr)<2.5")
#,("Ratio","","")
]
#&&((EcalTrackDZAtDR0p3MatchPtCorr<1&&EcalTrackDZAtDR0p3MatchPtCorr>-3&&EcalEta<0&&EcalEta>-1.6)||(EcalTrackDZAtDR0p3MatchPtCorr<-1&&EcalTrackDZAtDR0p3MatchPtCorr<3&&EcalEta>0&&EcalEta<1.6)||(abs(EcalTrackDLAtDR0p3MatchPtCorr)<0.4&&abs(EcalEta)>1.6))")
#,("Ratio","","")



h1d = []

for infile in inputs:
    inputfile=TFile(infile[0],"READ")
    tr=inputfile.Get("rootTupleTree/tree")
    #tr.Show()
    tr.Draw(">>skim",globalselection,"entrylist");
    skim = gDirectory.Get("skim");
    tr.SetEntryList(skim);
    #
    for i,ivar in enumerate(variables):
        if "Ratio" in ivar[0] :
            empty=TH1D(); empty.SetDirectory(0)
            #h1d.append(empty.Clone("histo_"+ivar[0]))
            h1d.append(empty)
            del empty
        if "Ratio" not in ivar[0] :
            print "Selection: ",ivar[0]+">>histo_"+ivar[0]+"("+ivar[1]+")"
            tr.Draw(ivar[0]+">>histo_"+ivar[0]+"("+ivar[1]+")",ivar[2]); histo=gDirectory.Get("histo_"+ivar[0]); 
            histo.SetDirectory(0)
            #h1d.append(histo.Clone(ivar[0]))
            h1d.append(histo)
            print "histo.GetEntries(): ",histo.GetEntries()
            del histo

print "size of h1d: ",len(h1d)


plotNumDenum = True
#plotNumDenum = False
can=plotSettings.CreateCanvas1D("can")
for i,ivar in enumerate(variables):
    if "Ratio" not in ivar[0] and plotNumDenum: 
        leg = TLegend(.55,.22,.77,.43); leg.SetBorderSize(0); leg.SetFillColor(0); leg.SetFillStyle(0); leg.SetTextFont(42); leg.SetTextSize(0.035)
        for j,infile in enumerate(inputs):
            index= i+j*len(variables)
            print "index: ",index
            #
            h1d[index].SetDirectory(0)
            can.SetLogx(0); can.SetLogy(1); can.SetLogz(1); can.cd()
            plotSettings.SetHistoStyle(h1d[index])
            h1d[index].SetFillStyle(0)
            h1d[index].SetLineColor(j+1)
            h1d[index].GetYaxis().SetRangeUser(0.5,100000)
            h1d[index].GetXaxis().SetTitleOffset(0.95);  h1d[index].GetYaxis().SetTitleOffset(1.13)
            h1d[index].GetYaxis().CenterTitle(0); h1d[index].GetXaxis().CenterTitle(0)
            h1d[index].GetXaxis().SetTitle(ivar[0])
            h1d[index].GetYaxis().SetTitle("Count")
            h1d[index].SetTitle(globalselection+"&&"+ivar[2])
            if index < len(variables) : h1d[index].Draw("HIST")
            else: h1d[index].Draw("HISTSame")
            leg.AddEntry(h1d[index],infile[1],"L")
        if len(variables)==1 : can.Print("efficiencyStudies1D_"+studyName+".pdf")
        else                 :
            if i==0          : can.Print("efficiencyStudies1D_"+studyName+".pdf(")
            elif (i==len(variables)-1 and variables[-1][0]!="Ratio") or (i==len(variables)-2 and variables[-1][0]=="Ratio"): can.Print("efficiencyStudies1D_"+studyName+".pdf)")
            else             : can.Print("efficiencyStudies1D_"+studyName+".pdf")

r1d = []
pagecount=0
for i,ivar in enumerate(variables):
    if "Ratio" in ivar[0]:
        leg = TLegend(.55,.22,.77,.43); leg.SetBorderSize(0); leg.SetFillColor(0); leg.SetFillStyle(0); leg.SetTextFont(42); leg.SetTextSize(0.035)
        for j,infile in enumerate(inputs):
            index= i+j*len(variables)
            print "index: ",index
            print "Ratio j: ",j
            h1d[index].SetDirectory(0)
            #can.Clear()
            can.SetLogx(0); can.SetLogy(0); can.SetLogz(1); can.cd()
            #plotSettings.SetHistoStyle(h1d[index])
            ratio=TGraphAsymmErrors()            
            ratio.Divide(h1d[index-1],h1d[index-2],"cp")
            ratio.SetMarkerStyle(9)
            ratio.SetMarkerSize(0.7)
            h1d[index-1].GetYaxis().SetRangeUser(0,1)
            h1d[index-1].GetYaxis().SetTitle("Efficiency")
            h1d[index-1].GetXaxis().SetTitle(variables[i-1][0])
            h1d[index-1].SetTitle(globalselection+"&&"+variables[i-1][2])
            ratio.SetMarkerColor(j+1)
            ratio.SetLineColor(j+1)
            r1d.append(ratio.Clone("Ratio"+str(index)))
            if index < len(variables) : h1d[index-1].Reset("MICES")
            if index < len(variables) : h1d[index-1].Draw("HIST")
            #ratio.Draw("PEsame")
            r1d[-1].Draw("PEZsame")
            leg.AddEntry(r1d[-1],infile[1],"LEP")
            leg.Draw()
        pagecount+=1
        if   pagecount==1                            : can.Print("efficiencyStudiesR1D_"+studyName+".pdf(")
        elif i==len(variables)-1 and j==len(inputs)-1: can.Print("efficiencyStudiesR1D_"+studyName+".pdf)")
        else                                         : can.Print("efficiencyStudiesR1D_"+studyName+".pdf")

        
##############################################

variables2D = [
 ("EcalTrackDRMin:EcalPt","120,0,120,1000,-0.50,0.50","abs(EcalEta)<2.5")
,("EcalTrackDRMin:EcalEta","50,-2.5,2.5,1000,-0.50,0.50","EcalPt>10")

,("EcalTrackDPhiAtDR0p3MatchPt:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<2.5")
,("EcalTrackDPhiAtDR0p3MatchPtCorr:EcalEta","50,-2.5,2.5,600,-0.15,0.15","EcalPt>10")

,("EcalTrackDPhiAtDR0p3MatchPt:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.6")
,("EcalTrackDPhiAtDR0p3MatchPtCorr:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.6")
,("EcalTrackDZAtDR0p3MatchPt:EcalPt","120,0,120,200,-10,10","abs(EcalEta)<1.6")
,("EcalTrackDZAtDR0p3MatchPtCorr:EcalPt","120,0,120,200,-10,10","abs(EcalEta)<1.6")
,("EcalTrackDZAtDR0p3MatchPtCorr:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")

,("EcalTrackDPhiAtDR0p3MatchPt:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)>1.6&&abs(EcalEta)<2.5")
,("EcalTrackDPhiAtDR0p3MatchPtCorr:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)>1.6&&abs(EcalEta)<2.5")
,("EcalTrackDLAtDR0p3MatchPt:EcalPt","120,0,120,200,-10,10","abs(EcalEta)>1.6&&abs(EcalEta)<2.5")
,("EcalTrackDLAtDR0p3MatchPtCorr:EcalPt","120,0,120,200,-2,2","abs(EcalEta)>1.6&&abs(EcalEta)<2.5")
,("EcalTrackDLAtDR0p3MatchPtCorr:EcalEta","50,-2.5,2.5,200,-2,2","abs(EcalEta)>1.6&&abs(EcalEta)<2.5&&EcalPt>10")
]


variables2D = [
 ("EcalTrackDPhiAtDR0p3MatchPt:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")
,("EcalTrackDPhiAtDR0p3MatchPtCorr1:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")
,("EcalTrackDPhiAtDR0p3MatchPtCorr2:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")
,("EcalTrackDPhiAtDR0p3MatchPtCorr3:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")
,("EcalTrackDPhiAtDR0p3MatchPtCorr4:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")
,("EcalTrackDPhiAtDR0p3MatchPtCorr5:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")
,("EcalTrackDPhiAtDR0p3MatchPtCorr6:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")
,("EcalTrackDPhiAtDR0p3MatchPtCorr7:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")
,("EcalTrackDPhiAtDR0p3MatchPtCorr8:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")
,("EcalTrackDPhiAtDR0p3MatchPtCorr9:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")
,("EcalTrackDPhiAtDR0p3MatchPtCorr10:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")
,("EcalTrackDPhiAtDR0p3MatchPtCorr11:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")
,("EcalTrackDPhiAtDR0p3MatchPtCorr12:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")
,("EcalTrackDPhiAtDR0p3MatchPtCorr13:EcalPt","120,0,120,600,-0.15,0.15","abs(EcalEta)<1.0")

,("EcalTrackDZAtDR0p3MatchPtCorr:EcalPt","120,0,120,200,-10,10","abs(EcalEta)<1.6")
,("EcalTrackDZAtDR0p3MatchPtCorr:EcalPt","120,0,120,200,-10,10","abs(EcalEta)<1.0&&EcalEta<0")
,("EcalTrackDZAtDR0p3MatchPtCorr:EcalPt","120,0,120,200,-10,10","abs(EcalEta)<1.0&&EcalEta>0")

,("EcalTrackDZAtDR0p3MatchPtCorr1:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr2:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr3:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr4:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr5:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr6:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr7:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr8:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr9:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr10:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr11:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")

,("EcalTrackDZAtDR0p3MatchPtCorr61:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr62:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr63:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr64:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr65:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr66:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr67:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr68:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
,("EcalTrackDZAtDR0p3MatchPtCorr69:EcalEta","50,-2.5,2.5,200,-10,10","abs(EcalEta)<1.6&&EcalPt>10")
]


can=plotSettings.CreateCanvas1D("can")


h2d = {}
h2dkeys = []
for j,infile in enumerate(inputs):
    inputfile=TFile(infile[0],"READ")
    tr=inputfile.Get("rootTupleTree/tree")
    #tr.Show()
    tr.Draw(">>skim",globalselection,"entrylist");
    skim = gDirectory.Get("skim");
    tr.SetEntryList(skim);
    #
    for i,ivar in enumerate(variables2D):
        print "Selection: ",ivar[0]+">>histo_"+ivar[0]+"("+ivar[1]+")","("+ivar[2]+")"
        tr.Draw(ivar[0]+">>histo_"+str(i)+":"+ivar[0]+":"+infile[1]+"("+ivar[1]+")","("+ivar[2]+")"); 
        histo=gDirectory.Get("histo_"+str(i)+":"+ivar[0]+":"+infile[1]);
        histo.SetDirectory(0)
        histo.SetTitle(ivar[2])
        h2d[str(i)+":"+ivar[0]+":"+infile[1]] = histo.Clone(ivar[0]+":"+infile[1])
        h2d[str(i)+":"+ivar[0]+":"+infile[1]].SetDirectory(0)
        h2dkeys.append(str(i)+":"+ivar[0]+":"+infile[1])
        del histo

can2=plotSettings.CreateCanvas1D("can2",750,650)
for i,key in enumerate(h2dkeys):
    print "key: ",key
    print "h2d[key].GetEntries(): ",h2d[key].GetEntries()
    can2.SetLogx(0); can2.SetLogy(0); can2.SetLogz(1); can2.cd()
    title=h2d[key].GetTitle()
    plotSettings.SetHistoStyle(h2d[key])
    h2d[key].GetYaxis().SetRangeUser(-9999,9999)
    h2d[key].GetXaxis().SetRangeUser(-9999,9999)
    h2d[key].GetXaxis().SetTitleOffset(0.95);  h2d[key].GetYaxis().SetTitleOffset(1.0)
    h2d[key].GetYaxis().CenterTitle(0); h2d[key].GetXaxis().CenterTitle(0)
    h2d[key].GetYaxis().SetTitleSize(0.04)
    h2d[key].GetXaxis().SetTitleSize(0.04)
    h2d[key].GetXaxis().SetTitleOffset(1.40)
    h2d[key].GetYaxis().SetTitleOffset(1.70)
    h2d[key].GetYaxis().SetTitle(key.split(":")[1])
    h2d[key].GetXaxis().SetTitle(key.split(":")[2])
    h2d[key].SetTitle(title)
    h2d[key].Draw("COLZ")
    leg = TLegend(.55,.22,.77,.43); leg.SetBorderSize(0); leg.SetFillColor(0); leg.SetFillStyle(0); leg.SetTextFont(42); leg.SetTextSize(0.035)
    leg.AddEntry(h2d[key],key.split(":")[3],"")
    leg.Draw()
    if len(h2d)==1 : can2.Print("efficiencyStudies2D_"+studyName+".pdf")
    else          :
        if i==0          : can2.Print("efficiencyStudies2D_"+studyName+".pdf(")
        elif i<len(h2d)-1 : can2.Print("efficiencyStudies2D_"+studyName+".pdf")
        else             : can2.Print("efficiencyStudies2D_"+studyName+".pdf)")
            
exit()


